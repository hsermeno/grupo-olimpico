#define INF -1;

// s es el vertice inicial

queue<int> q;
q.push(s);

vector<int> color(V, INF);
color[s] = 0;
bool esBipartita = true;

// similar a la BFS original, pero agregando
// la bandera esBipartita
while (!q.empty() & esBipartita) {
    int u = q.front();
    int aristas = (int)grafo[u].size();
    q.pop();
    
    for (int j = 0; j < aristas; j++) {
        vector<int, int> v = grafo[u][j];
        
        if (color[v.first] == INF) { 
            // en lugar de recordarnos de la distancia
            // usamos a {0, 1} como colores
            color[v.first] = 1 - color[u]; 
            q.push(v.first);
        } else if (color[v.first] == color[u]) { 
            // u & v tienen el mismo color!
            esBipartita = false;
            break;
        }
    }
}

if (esBipartita) {
    // el grafo es bipartito (bicolorable)
}
