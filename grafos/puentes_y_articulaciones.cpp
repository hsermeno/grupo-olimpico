#define NO_VISITADO -1

// asumo uso de variables globales
vector<int> disc;
vector<int> disc_menor;
vector<int> padre;
vector<bool> puntos_articulacion;
vector<pair<int, int> > puentes;
int raiz, hijosRaiz;

void dfs(int u) {
    // iniciamos con el tiempo de descubrimiento
    // de U y el menor tiempo de descubrimiento
    // al que podemos llegar desde U como el 
    // tiempo actual
    disc[u] = disc_menor[u] = tiempo++;
    int edges = (int)grafo[u].size();

    // recorremos todos los vertices de U
    for (int j = 0; j < edges; j++) {
        int v = grafo[u][j];

        if (disc[v] == NO_VISITADO) {
            padre[v] = u;

            // caso especial para llevar
            // conteo de los hijos del vertice raiz
            if (u == raiz) {
                hijosRaiz++;
            }

            dfs(v);

            // si del arbol que inicia en v no podemos llegar a
            // uno de los ancestros de u, u es punto
            // de articulación.
            if (disc[u] <= disc_menor[v]) {
                puntos_articulacion[u] = true;
            }

            // si del arbol que inicia en v, no existe una arista
            // que vaya a u o a uno de sus ancestros, (u, v) es
            // un puente
            if (disc[u] < disc_menor[v]) {
                puentes.push_back({u, v});
            }

            // actualizamos el menor tiempo de descubrimiento de u
            // en caso que el arbol que comienza en v haya tenido un 
            // back edge a algun ancestro de u
            disc_menor[u] = min(disc_menor[u], disc_menor[v]);
        } else {
            if (v != padre[u]) {
                // tenemos un back edge, por lo que actualizamos
                // el menor tiempo de descubrimiento al que podemos
                // llegar a partir del vertice actual
                disc_menor[u] = min(disc_menor[u], disc_menor[v]);
            }
        }
    }
}

// en algun otro lugar
disc.assign(V, NO_VISITADO);
disc_menor.assign(V, 0);
padre.assign(V, 0);

puntos_articulacion.assign(V, false);
puentes.clear();
tiempo = 0;

for (int i = 0; i < V; i++) {
    if (disc[i] == NO_VISITADO) {
        raiz = i;
        hijosRaiz = 0;

        dfs(i);

        // caso especial: la raiz es punto de
        // articulación si tiene al menos dos
        // hijos
        if (hijosRaiz > 1) {
            puntos_articulacion[raiz] = true;
        }
    }
}
