//
// Orden topológico utilizando el algoritmo de Kahn
//
// asumimos grafo sin peso en lista de adyacencia
//
// vector<vector<int> > grafo; 
//

int indegree[V];

memset(indegree, 0, sizeof indegree);

//
// calculamos los indegree de todos los vertices O(E)
//
for (int u = 0; u < V; u++) {
    for (int j = 0; j < (int)grafo[u]; j++) {
        indegree[grafo[u][j]]++;
    }
}

// puede ser un priority_queue en caso
// se necesite que los nodos con menor
// numeración aparezcan antes
queue<int> q;
vector<int> topo;

// ponemos en cola los vertices con
// indegree 0
for (int u = 0; u < V; u++) {
    if (indegree[u] == 0) {
        q.push(u);
    }
}

while (!q.empty()) {
    int u = q.front();
    q.pop();

    // se recalcula el indegree de los vertices
    // a los que se puede llegar desde u
    for (int j = 0; j < (int)grafo[u].size(); j++) {
        int v = grafo[u][j];

        indegre[v]--;

        // si el indegree se hace 0, lo ingresamos
        // a la cola
        if (indegree[v] == 0) {
            q.push(v);
        }
    }

    topo.push_back(u);
}

// si al final topo.size() < V, el grafo no es un DAG

