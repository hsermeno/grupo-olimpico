//
// Orden topológico utilizando un DFS modificado
//
// Asume como entrada un grafo con pesos
// en una lista de adyacencia:
//
// vector<vector<pair<int, int> > > grafo;
//

vector<int> topo;
int visitado[V];

void dfs(int u) {
    visitado[u] = 1;

    for (int j = 0; j < (int)grafo[u].size(); j++) {
        pair<int, int> grafo[u][j];

        if (!visitado[v.first]) {
            dfs(v.first);
        }
    }

    // el único cambio con respecto al DFS normal
    topo.push_back(u);
}

// en otro lugar...
topo.clear();

memset(visitado, 0, sizeof visitado);

for (int i = 0; i < V; i++) {
    if (!visitado[i]) {
        dfs(i);
    }
}

// topo contiene el orden topológico en orden inverso
