// s es el vertice inicial

vi dist(V, INF);
dist[s] = 0;

priority_queue<ii, vector<ii>, greater<ii> > pq;

pq.push({0, s});

while (!pq.empty()) {
    ii sig = pq.top();
    pq.pop();

    int d = sig.first;
    int u = sig.second;

    // este if es muy importante
    if (d > dist[u]) continue;

    for (int j = 0; j < (int)grafo[u].size(); j++) {
        ii v = grafo[u][j];

        if (dist[u] + v.first < dist[v.second]) {
            // "relajamos" la distancia
            dist[v.second] = dist[u] + v.first;

            pq.push({ dist[v.second], v.second });
        }
    }
}
