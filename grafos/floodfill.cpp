//
// Floodfill
//
// asume:
// int matriz[R][C];
//

// vecinos en S,SE,E,NE,N,NO,O,SO
int dr[] = {1,1,0,-1,-1,-1, 0, 1}; 
int dc[] = {0,1,1, 1, 0,-1,-1,-1}; 

// devuelve el tamaño del componente
int floodfill(int r, int c, char c1, char c2) {
    // fuera de la matriz
    if (r < 0 || r >= R || c < 0 || c >= C) {
        return 0;
    }

    // no tiene el color 1
    if (matriz[r][c] != c1) {
        return 0;
    }

    int ans = 1; 

    // se marca con c2 para evitar ciclos
    matriz[r][c] = c2;
    
    // dfs en cada uno de sus vecinos
    for (int d = 0; d < 8; d++) {
        ans += floodfill(r + dr[d], c + dc[d], c1, c2);

    }
    
    return ans;
}
