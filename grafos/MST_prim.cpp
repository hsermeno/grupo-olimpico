// asume grafo como lista de adyacencia

// para poder evitar ciclos
vector<bool> tomado;
// para contar el numero de vertices procesados
int tomados;

// 
priority_queue<pair<int, int> > pq;


void procesar(int u) {
    int aristas = (int)grafo[u].size();

    tomado[u] = true;

    for (int j = 0; j < aristas; j++) {
        pair<int, int> v = grafo[u][j];

        if (!tomado[v.first]) {
            // se guardan con signos invertidos
            // para que la PQ nos devuelva en
            // orden no descendiente
            pq.push({-v.second, -v.first});
        }
    }
}

int V = (int)grafo.size();

// en algun otro lugar
tomado.assign(V, false);

// iniciamos en el vertice 0
process(0);

costo_mst = 0;
tomados = 1;

while (!pq.empty() && tomados < V) {
    pair<int, int> sig = pq.top();

    pq.pop();

    u = -sig.second;
    w = -sig.first;

    if (!tomado[u]) {
        tomados++;
        costo_mst += w;
        procesar(u);
    }
}

// al final, si tomados < V, el grafo estaba desconectado

cout << "El costo MST (Prim) es " << costo_mst << endl;