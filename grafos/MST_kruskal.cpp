// Vertices contiene la lista de todas las aristas como 
//
// vector<pair<int, pair<int, int> > > aristas;
//
// cada pareja es del tipo {w, {u, v}} donde W es el peso, 
// U y V los vertices unidos por la arista.
//
// Notar que si {w, {u, v}} está en la lista, {w, {v, u}}
// no debe estarlo! es decir, cada arista aparece únicamente
// una vez en la lista.
//
// Se asume la implementación de la estructura Union-Find
// Sisjoint Sets en la clase UnionFind


// comenzamos ordenando los vertices por peso O(VlogV):
sort(aristas.begin(), aristas.end());

int costo_mst = 0;
int E = (int)aristas.size();

// utilizamos la estructura Union-Find
// todos los V son conjuntos disjuntos inicialmente
UnionFind UF(V);

// O(E):
for (int i = 0; i < E; i++) {
    pair<int, pair<int, int>> sig = aristas[i];

    // si no estan en el mismo conjunto, no estamos creando un
    // ciclo, por lo que esta arista es parte del MST
    if (!UF.isSameSet(sig.second.first, sig.second.second)) {
        costo_mst += sig.first;

        // hacemos la union de los conjuntos correspondientes
        // a los dos vertices
        UF.unionSet(sig.second.first, sig.second.second);
    }
}

// al final, si todos los vertices están en el mismo conjunto
// es porque el grafo era conectado.

cout << "El costo MST (Kruskal's) es " << costo_mst << endl;
