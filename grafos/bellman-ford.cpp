// ALGORITMO Bellman Ford
// para el problema Single Source Shortest Path (SSSP)
// O(VE)

vi dist(V, INF);
dist[s] = 0;

for (int i = 0; i < V - 1; i++) {
    // "relajamos" todos los vertices V-1 veces
    for (int u = 0; u < V; u++) {
        for (int j = 0; j < (int)grafo[u].size(); j++) {
            ii v = grafo[u][j];
            // aqui se puede guardar el spanning tree de ser necesario
            // ...
            // relajamos
            dist[v.second] = min(dist[v.second], dist[u] + v.first);
        }
    }
}

// este algorimo nos permite detectar ciclos negativos
// corriendo una ultima vez la relajacion de los vertices
// si es posible encontrar un camino menor, es porque hay
// un ciclo negativo
bool cicloNegativo = false;
for (int u = 0; !cicloNegativo && u < V; u++) {
    for (int j = 0; j < (int)grafo[u].size(); j++) {
        ii v = grafo[u][j];

        // si esto es posible todavia, tenemos un ciclo negativo
        if (dist[v.second] > dist[u] + v.first) {
            cicloNegativo = true;
            break;
        }
    }
}

if (cicloNegativo) {
    // ...
}