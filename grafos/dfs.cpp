typedef pair<int,int> edge;
typedef vector<edge> vii;
typedef vector<vii> graph;

#define VISITADO 1
#define NOVISITADO 0

// global por simplicidad en este ejemplo
// puede ser inicializado en otro lugar
// y pasado por referencia al algoritmo
// contiene el estado de cada vertice
vector<int> visitado;


// Esta función recorre todos los elementos en 
// la lista de adjacencia exactamente una vez.
// Dado que el tamaño de la lista es 2|E| + |V|
// el algoritmo se ejecuta en O(V+E)
void dfs(const graph &grafo, int u) {
    // marcar el nodo de origen como visitado
    visitado[u] = VISITADO;

    // recorrer todas las aristas salientes del
    // nodo actual
    for (int j = 0; j < grafo[u].size(); j++) {
        edge v = grafo[u][j];

        // si el nodo adjacente no ha sido visitado
        // llamar la funcion recursivamente
        if (visitado[v.first] == NOVISTADO) {
            dfs(v.first);
        }
    }
}

// en algun lugar...
visitado.assign(grafo.size(), NOVISITADO);
