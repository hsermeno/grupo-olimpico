// nuestra definicion de grafo
// como lista de adyacencia
typedef pair<int,int> edge;
typedef vector<edge> vii;
typedef vector<vii> graph;

#define INFINITO -1

// ... codigo aqui ...

// vector con la distancia (en numero de aristas)
// hacia cada vertice a partir de s
vector<int> d(grafo.size(), INFINITO);

// la distancia de s a s es 0
d[s] = 0;

// la cola que contiene a los
// vertices que deben ser visitados
queue<int> q;

// el primer vertice a visitar es el origen
q.push(s);

// mientras tengamos vertices que visitar...
while (!q.empty()) {
    // obtener el vertice actual
    int u = q.front();
    // y eliminarlo de la lista
    q.pop();

    // recorrer cada arista que sale del vertice
    for (int j = 0; j < (int)grafo[u].size(); j++) {
        edge v = grafo[u][j];

        if (d[v.first] == INFINITO) {
            // guardamos la distancia que tomó
            // legar a este nodo
            d[v.first] = d[u] + 1;

            // guardar el nodo adyacente para
            // ser visitado
            q.push(v.first);
        }
    }
}

// d guarda la menor distancia desde el nodo
// inicial hasta cada nodo del grafo. 
// Esto es cierto en grafos sin pesos
